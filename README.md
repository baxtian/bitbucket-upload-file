# Bitbucket Upload File

**Create an archive and update it to bitbucket.**

This library allows to upload an archive to the downloads directory in Bitbucket.

## Access token

To use Bitbucket API you have to create an **access token** with write permissions.

1. Go to the **Repository settings** in the left menu.
1. Look for the item **Access tokens** inside the section labeled **Security**.
1. Create a new **Access token** with permisions to *write* in the **Repository**.
1. Copy the access token.

### Set the environment

At root of your project create or modify the .env and add these fields:

	WORKSPACE=your-bitbucket-name
	PACKAGE=package-name
	ACCESS_TOKEN=xxxxxxxx

*Workspace* and *Package* are parte of the URL to your Bitbucket repository.
https://bitbucket.org/{workspace}/{package}/src/master/

## How to use with composer

### Install the library

Run the next command

	composer install baxtian/bitbucket-upload-file

Add to the *composer.json* file the *scripts* and *archive* elements.

	{
		.
		.
		.
		"scripts": {
			.
			.
			.
			"dist": "composer archive --format=zip",
			"release": [
				"@dist",
				"BitbucketUploadFile\\App::archive"
			]
		},
		"archive": {
			"exclude": [
				"/.vscode/",
				"/.env",
				"/.env.example",
				"/vendor/",
				".gitignore",
				"composer.*",
				"README.md",
				"*.zip"
			]
		}
		.
		.
		.
	}

Pay attention to the *archive* section in *composer.json*. Read the [documentation](https://getcomposer.org/doc/04-schema.md#archive) to understand how to add or remove elements to the archive.

### How to use this?

run this command to create your archive

	composer dist

run this command to upload your archive

	composer release

## NPM

### Install the library

Run the next command

	npm install bitbucket-upload-file

Add to the *package.json* file the *scripts*.

	{
		.
		.
		.
		"scripts": {
			.
			.
			.
			"build": "your own build command",
			"dist": "npm run build && node node_modules/bitbucket-upload-file/scripts/archive.js",
			"release": "npm run dist && node node_modules/bitbucket-upload-file/scripts/upload.js"
		},
		.
		.
		.
	}

### Additionall fields in environment

Add to the .env file the next information:

	DIST_DIR=dist

*DistDir* is the directory where the version to be distributed will be created, so the file will have the files in this directory.

### How to use this?

run this command to create your archive

	composer dist

run this command to upload your archive

	composer release
