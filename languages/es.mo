��          T      �       �      �      �   +   �            9     P  �  m      a  !   �  *   �  )   �  "   �                                              Archive %s uploaded. Archive hasn't been uploaded. Archive to be uploaded hasn't been created. No access token in the env file. No composer directory. Uploading file to Bitbucket. Project-Id-Version: bbuf
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-09-22 17:47-0500
Last-Translator: 
Language-Team: Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: $i18n->gettext
X-Poedit-Basepath: ..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: src
X-Poedit-SearchPath-1: languages
 El archivo %s ha sido publicado. El archivo no pudo ser publicado. El archivo a publicar aun no se ha creado. No hay token de acceso en el archivo env. Este no es un directorio composer. Subiendo el archivo a Bitbucket. 