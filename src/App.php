<?php

namespace BitbucketUploadFile;

use Unirest\Request\Body;
use Unirest\Request;
use Dotenv\Dotenv;
use Codedungeon\PHPCliColors\Color;
use BitbucketUploadFile\Translate\MoTranslator;
use Locale;
use Exception;

/**
 * BitbucketUploadFile\App
 * @codeCoverageIgnore
 */
class App
{
	public const VERSION = '0.1.5';

	private static function i18n()
	{
		// i18n
		$i18n = MoTranslator::get_instance();
		$i18n->setLocale(Locale::getDefault(), __DIR__ . '/../languages/');
	}

	private static function submit($name, $file, $access_token)
	{
		// Enviar archivo usando Unirest
		$headers = [
			'Authorization' => 'Bearer ' . $access_token,
		];
		$files = ['files' => $file];
		$body  = Body::multipart([], $files);

		$url = sprintf('https://api.bitbucket.org/2.0/repositories/%s/downloads', $name);

		/** @var Response */
		$response = Request::post($url, $headers, $body);

		if ($response->code != 201) {
			throw new Exception(__("Archive hasn't been uploaded. Check your access token."));
		}
	}

	public static function archive()
	{
		// Inicializar i18n
		self::i18n();

		// Inicializar entorno
		$dotenv = Dotenv::createImmutable(getcwd());
		$dotenv->safeLoad();
		if (!isset($_ENV['ACCESS_TOKEN'])) {
			throw new Exception(__('No access token in the env file.'));
		}

		// Inicialisar datos composer
		$config = getcwd() . DIRECTORY_SEPARATOR . 'composer.json';
		if (!file_exists($config)) {
			throw new Exception(__('No composer directory.'));
		}
		$json     = file_get_contents($config);
		$composer = json_decode($json, false);

		// Archivo zip
		$zip_file = sprintf('%s-%s.zip', $composer->name, $composer->version);
		$zip_file = str_replace('/', '-', $zip_file);
		if (!file_exists($zip_file)) {
			throw new Exception(__("Archive to be uploaded hasn't been created."));
		}

		// Informar que vamos a subir el archivo
		$message = __('Uploading file to Bitbucket.');
		echo  Color::GREEN . $message . Color::RESET . PHP_EOL;

		self::submit($composer->name, $zip_file, $_ENV['ACCESS_TOKEN']);

		$message = sprintf(__('Archive %s uploaded.'), $zip_file);
		echo  $message . ' ' . Color::GREEN . '✔' . Color::RESET . PHP_EOL;
	}
}
