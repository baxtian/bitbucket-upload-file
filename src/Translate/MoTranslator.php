<?php

/**
 * Copyright (C) 2023 baxtian.echeverry@gmail.com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 */

namespace BitbucketUploadFile\Translate;

use PhpMyAdmin\MoTranslator\Loader;
use PhpMyAdmin\MoTranslator\Cache\InMemoryCache;
use PhpMyAdmin\MoTranslator\MoParser;
use PhpMyAdmin\MoTranslator\Translator;
use Baxtian\SingletonTrait;
use Locale;

/**
 * Scripts
 * @codeCoverageIgnore
 */
class MoTranslator
{
	use SingletonTrait;

	private $translator;

	public function setLocale($location, $dir)
	{
		$listLocales = Loader::listLocales(Locale::getDefault());
		$mo_file     = null;
		foreach ($listLocales as $locale) {
			$aux = $dir . DIRECTORY_SEPARATOR . $locale . '.mo';
			if (file_exists($aux)) {
				$mo_file = $aux;
			}
		}

		$cache            = new InMemoryCache(new MoParser($mo_file));
		$this->translator = new Translator($cache);
		$this->loadFunctions();
	}

	public function loadFunctions(): void
	{
		require_once __DIR__ . '/MoTranslatorFunctions.php';
	}

	public function gettext($string)
	{
		return $this->translator->gettext($string);
	}

	public function pgettext($string, $context)
	{
		return $this->translator->pgettext($context, $string);
	}
}
