/**
 * Script para crear archivo del directorio a publicar.
 * 
 * *******************************************
 * Debe crear un archivo .env con los valores:
 * 
 * WORKSPACE=your-bitbucket-name
 * PACKAGE=package-name
 * DIST_DIR=dist
 * ACCESS_TOKEN=xxxxxxxx
 * 
 * Los valores workspace y package corresponden a los elementos de la URL del repositorio
 * https://bitbucket.org/{workspace}/{package}/src/master/
 * 
 * DIST_DIR corresponde al directorio en donde queda la compilación a publicar.
 * 
 * ACCESS_TOKEN es un token que se debe crear en Bitbucket con permisos de escritura
 * en el repositorio.
 * 
 * *******************************************
 * Incluya en el archivo package.json el campo "version". Si no agrega el campo no se agregará campo de
 * versión al final del nombre del archivo a subir.
 * 
 */

// Requerimientos
const fs = require('fs');
const dotenv = require('dotenv').config();
const archiver = require('archiver');
const dir = process.cwd();
const package = require(dir + '/package.json')

// Datos
const access_token = process.env.ACCESS_TOKEN;
const repo_slug = process.env.PACKAGE;
const workspace = process.env.WORKSPACE;
const dist_dir = process.env.DIST_DIR;
const version = package.version ?? false;

// Nombre del archivo
const filename = (version) ? workspace + '-' + repo_slug + '-' + version : workspace + '-' + repo_slug;

// Crea el archivo al que se anexará la informción.
const output = fs.createWriteStream(dir + '/' + filename  + '.zip');
const archive = archiver('zip', {
  zlib: { level: 9 } // Sets the compression level.
});

// Atender todas las acciones al escribir en el archivo
// El evento 'close' se activa solo cuando un descriptor está involucrado
output.on('close', function() {
  console.log('The archive has been created with name '+ filename +'.zip');
});

// Atender alertas
archive.on('warning', function(err) {
  if (err.code === 'ENOENT') {
    // log warning
  } else {
    // throw error
    throw err;
  }
});

// Atender errores
archive.on('error', function(err) {
  throw err;
});

// Dirigir los datos al archivo
archive.pipe(output);

// Agregar el directorio a distibuir en la raiz
archive.directory( dist_dir + '/', false);

// Finalizar el proceso
archive.finalize();
