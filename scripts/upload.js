/**
 * Script para crear subir el archivo de distibución a bitbucket.
 * 
 * *******************************************
 * Debe crear un archivo .env con los valores:
 * 
 * WORKSPACE=your-bitbucket-name
 * PACKAGE=package-name
 * DIST_DIR=dist
 * ACCESS_TOKEN=xxxxxxxx
 * 
 * Los valores workspace y package corresponden a los elementos de la URL del repositorio
 * https://bitbucket.org/{workspace}/{package}/src/master/
 * 
 * DIST_DIR corresponde al directorio en donde queda la compilación a publicar.
 * 
 * ACCESS_TOKEN es un token que se debe crear en Bitbucket con permisos de escritura
 * en el repositorio.
 * 
 * *******************************************
 * Incluya en el archivo package.json el campo "version". Si no agrega el campo no se agregará campo de
 * versión al final del nombre del archivo a subir.
 * 
 */

// Requerimientos
const fs = require('fs');
const dotenv = require('dotenv').config();
const axios = require('axios')
const dir = process.cwd();
const package = require(dir + '/package.json')

// Datos
const access_token = process.env.ACCESS_TOKEN;
const repo_slug = process.env.PACKAGE;
const workspace = process.env.WORKSPACE;
const version = package.version;

// Nombre del archivo
const filename = (version) ? workspace + '-' + repo_slug + '-' + version : workspace + '-' + repo_slug;

// Path
const path = dir + '/' + filename + '.zip';

try {
	// revisar si el archivo existe
	if (!fs.existsSync(path)) throw (new Error("File does not exists"));

	// Url para API de Bitbucket
	const baseUrl = `https://api.bitbucket.org/2.0/repositories/${workspace}/${repo_slug}/`;

	// Llamar la API en modo multi-part
	axios.post(
		baseUrl + 'downloads',
		{
			files: fs.createReadStream(path)
		},
		{
			headers: {
				'Content-Type': 'multipart/form-data',
				'Authorization': `Bearer ${access_token}`
			}
		}
	)
		.then(data => {
			console.log("File has been uploaded.")
		})
		.catch(error => {
			throw new TypeError("Error uploading file.");
		})
		.finally(() => {
			// always executed
		});

} catch (err) {
	const { message, error, headers, request, status } = err
	console.error(message);
}
